/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un bloc stator
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/*  PARAMETRES REGLABLES */
    
    // PARAMETRES APPELES DEPUIS D'AUTRES FICHIERS SCAD
    include <Rotor-4-Aimants-Donnees-Communes-V00-01.scad>;
    
    // PARAMÈTRES SPÉCIFIQUES
    
    HBilles = 7; // Hauteur du roulement à billes
    Hstatorpartbas = 95; // hauteur paroies de la partie basse du stator, hors socle, hors chapeau
    EpParoieStator = 1; // épaisseur des paroies
    EntreRotorStator = 1.5; // écart entre paroies rotor et stator
    InterParoies = 10; // espace entre les paroies = épaisseur max des bobines
    Alveoli = 5; // Diamètre des alvéoles
    EntreAveol = 3; // matière entre alvéoles
    Pi = 3.14116; // Pi
    Dtrousfix1 = 5; // Diamètre des trous de fixation + 1
    Npattes = 6; //Nombre de pattes de fixation
    
    // PARAMETRES CALCULÉS
    HParoieInt = Hstatorpartbas - 10; //
    ParoieIntRayonInt = DEXTrotor + EntreRotorStator; // Rayon de la surface interne de la paroie interne
    ParoieIntRayonExt = ParoieIntRayonInt + EpParoieStator; // 
    ParoieExtRayonInt = ParoieIntRayonExt + InterParoies; //
    ParoieExtRayonExt = ParoieExtRayonInt + EpParoieStator; //
    NbAlveoles = ((2*Pi*ParoieIntRayonInt)/(Alveoli+EntreAveol)); //Nb alveol par rang
    NbRangAlveoles = Hstatorpartbas/(Alveoli*2) - 1; // Nb rang en hauteur
    Dpattes = Dtrousfix1 + 20; // Diamètre des pattes rondes de fixation

/*  DEFINITIONS DE MODULES */
    
    // PAROIS DU STATOR
            
        // Paroi Interne
            module ParoiInterne() {
                difference() {
                    color("blue")
                    translate([0,0,Hstatorpartbas/2])
                    cylinder(h = Hstatorpartbas, r =ParoieIntRayonExt, center = true);
                    
                    color("blue")
                    translate([0,0,Hstatorpartbas/2])
                    cylinder(h = Hstatorpartbas, r =ParoieIntRayonInt, center = true);
                    }
                }

        // Paroi Externe
            module ParoiExterne() {
                difference() {
                    color("pink")
                    translate([0,0,Hstatorpartbas/2])
                    cylinder(h = Hstatorpartbas, r =ParoieExtRayonExt, center = true);
                    
                    color("pink")
                    translate([0,0,Hstatorpartbas/2])
                    cylinder(h = Hstatorpartbas, r =ParoieExtRayonInt, center = true);
                    }
                }
                
        // Parois internes et externes
                module ParoisDouble() {
                    union() {
                        translate([0,0,epaissocl]) ParoiInterne();
                        translate([0,0,epaissocl]) ParoiExterne();
                        }
                    }
                    
    // SOCLE ET ÉLÉMENTS DE FIXATIONS

        // Socle
            module SocleStator() {
                    color("green")
                    translate([0,0,epaissocl/2])
                    cylinder(h = epaissocl, r =ParoieExtRayonExt, center = true);
                }
                    
        // Patte de fixation perpendiculaires à l'axe
            module patte() {
                difference() {
                    
                    // Des pattes en forme de jetons ronds 
                    color("yellow")
                    translate([ParoieExtRayonExt,0,epaissocl/2])
                    cylinder(h = epaissocl, d =Dpattes, center = true);
                    
                    // Des trous dans les pattes pour fixer
                    color("black")
                    translate([ParoieExtRayonExt+5,0,epaissocl/2])
                    cylinder(h = epaissocl, d =Dtrousfix1, center = true);
                    
                }
                }
                
        // Collier de pattes de fixation
            module pattes() {
                    for(np = [0 : (360/Npattes) : 360])
                        rotate(a=np, v=[0,0,1]) {                    
                            patte();
                        }
                }
        
                
    // ALVEOLES
            module alveolabeille() {
                color("firebrick")
                rotate(a=90, v=[0,1,0]) 
                    {
                    linear_extrude(height = InterParoies*2, center = true, convexity = 10, twist = 0) 
                    circle(Alveoli,$fn=6);
                }
            }
            module alveolia() {
                translate([(InterParoies*2)/2+30,0,(Alveoli/2)+(2*epaissocl)])
                alveolabeille();
                }
            module alveolias() {
//                for(variable = [start : increment : end])
//                for(i = [0 : (360/(NbAlveoles+(0))) : 360])
                for(i = [0 : (360/(NbAlveoles-12)) : 360])
                rotate (a=(i), v=[0,0,1]) {
                    alveolia();
                }
                }
            module alveolii() {
                for(n = [0 : 1 : NbRangAlveoles])
                    {
//                        rotate(a=(n*(Alveoli+(EntreAveol/2))), v=[0,0,1]) 
                        rotate(a=(n*(Alveoli+(EntreAveol*1.5))), v=[0,0,1]) 
                        translate([0,0,n*(Alveoli*2)])
                        alveolias();
                    }
                }

        // Socle Alveolé
            module ParoieAlveoles() {
                difference() {
                    ParoisDouble();
//                    alveolii(); 
                    }
                }
               
    // CLOISONS POUR LES BOBINES
        // CLOISON
            module cloison() {
                translate([0,((ParoieExtRayonExt-ParoieIntRayonInt)/2)+ParoieIntRayonInt,Hstatorpartbas/2])
                cube(size = [EpParoieStator,ParoieExtRayonExt-ParoieIntRayonInt,Hstatorpartbas], center = true);
                }
            module cloisons() {
                for(z = [0 : 360/6 : 360])
                rotate(a=(z), v=[0,0,1]) {
                    cloison();
                    }
                }
/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        ParoieAlveoles();
        SocleStator();
        pattes();
        cloisons();
        }
        alveolii();
}

/* FIN DE DOCUMENT */