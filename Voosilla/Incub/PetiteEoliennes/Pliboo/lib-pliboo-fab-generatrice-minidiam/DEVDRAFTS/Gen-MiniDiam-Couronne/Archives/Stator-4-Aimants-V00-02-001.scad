/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un bloc stator
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/*  IMPORTATIONS DEPUIS D'AUTRES FICHIERS */
    
    // PARAMETRES APPELES DEPUIS D'AUTRES FICHIERS SCAD
    include <Rotor-4-Aimants-Donnees-Communes-V00-01.scad>;
    
/*  PARAMETRES SP"CIFIQUES */
    Nbob = 3; // Nombre de bobines
    Hint = Nbob+45;

/*  DEFINITIONS DE MODULES */

    // ZONE DE PORTAGE DE LA BOBINE
        module portagebob() {
            union() {
                
                // Piliers
                color("red")
                translate([0,0,0])
                linear_extrude(height = HFluxTotal-HFluxCouv, center = false, convexity = 10) import(file = "lib-generatrice-minidiam-monocourronne-00-02-001.dxf", layer = "Pilier");
                
                // Paroie interne
                color("blue")
                translate([0,0,0])
                linear_extrude(height=HFluxHautBob, center = false, convexity = 10, twist = 0) import(file = "lib-generatrice-minidiam-monocourronne-00-02-001.dxf", layer = "ParoieInt");  

                color("yellow")
                translate([0,0,0])
                linear_extrude(height = HFluxRotor/2, center = false, convexity = 10, twist = 0) import(file = "lib-generatrice-minidiam-monocourronne-00-02-001.dxf", layer = "ParoieExt");  
            }
            
        }
        module portages() {
            for(i = [0 : (360/Nbob) : 360])
                rotate(a=i, v=[0,0,1]) 
                portagebob();
            }
            
        module soclage() {
                color("yellow")
                translate([0,0,0])
                linear_extrude(height = HFluxSocle, center = false, convexity = 10, twist = 0) import(file = "lib-generatrice-minidiam-monocourronne-00-02-001.dxf", layer = "socle");
            }            

        module roulemapoule() {
                color("yellow")
                translate([0,0,0])
                linear_extrude(height = HFluxSocle, center = false, convexity = 10, twist = 0) import(file = "lib-generatrice-minidiam-monocourronne-00-02-001.dxf", layer = "trouroulement");
            }            

        module logisroul() {
                color("pink")
                translate([0,0,3])
                linear_extrude(height = 4, center = false, convexity = 10, twist = 0) import(file = "lib-generatrice-minidiam-monocourronne-00-02-001.dxf", layer = "logeroulement");
            }                        
            
        module peton() {
                color("violet")
                translate([0,0,0])
                linear_extrude(height = 3, center = false, convexity = 10, twist = 0) import(file = "lib-generatrice-minidiam-monocourronne-00-02-001.dxf", layer = "pieds");
            }
        module petons() {
            for(i = [0 : (360/Nbob) : 360])
                rotate(a=i, v=[0,0,1]) 
                peton();
            
            }
/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        portages();
        soclage();
        logisroul();
        petons();
        }
        roulemapoule();
}

/* FIN DE DOCUMENT */