/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un bloc rotor pour loger 4 aimants
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/*  1.1 PARAMETRES REGLABLES */

/*  1.1.1 Paramètres d'écart de précision 
*   en fonction de l'imprimante utilisée */

    // écart de précision de l'impression 3D
    // à appliquer à rayon ou à une surface versus théorie
    ecart = 0.3; 

/*  1.1.2 Paramètres à saisir */
    
    // éléments transperçants
    Infini=200;
    
    // Logement de l'aimant parallépidède rectangle
    // posé debout à la vertical
    // Hauteur = plus grande arête
    // Largeur = seconde plus grande arête
    // Profondeur = plus petite arête
    Htheo=25; // Hauteur de l'aimant
    Ltheo=25; // Largeur de l'aimant
    Ptheo=13; // Profondeur de l'aimant
    
    // Nombre d'aimants
    Naimants=4; 

    // diamètre de l'axe
    diamaxe = 8;
    
    // épaisseur du socle
    epaissocl = 3;

    // Rayon du bloc
    Rbloc=30;
    
    // Espace au dessus de l'aimant
    EspaceHaimant=10;
    
    // Epaisseur des Bords
    Epbords = 1;
    
    // Matière à retirer du bloc dans les coins
    // Hauteur = plus grande arête
    // Largeur = seconde plus grande arête
    // Profondeur = plus petite arête
    Hcoin = Infini;
    Lcoin = Ptheo + 5;
    Pcoin = Ptheo + 5;
    
    // Matière centrale à retirer
    // un cube du la hauteur
    Hcoeur = Haimant;
    LPcoeur = 15;
    
    // trous de fixation éventuels
    Dfixtheo = 5;
    
    // Rayon de la couronne extérieure
    Rcour=31;
    

/*  1.1.3. Paramètres calculés */
    
    // Un double ecart
    e2=2*ecart;
    
    // Dimension du logement de l'aimant
    // la taille de l'aimant + écart pour faire passer l'aimant
    Haimant=Htheo+EspaceHaimant; // Hauteur du logement de l'aimant
    Laimant=Ltheo+e2; // Largeur du logement  de l'aimant
    Paimant=Ptheo+e2; // Profondeur du logement de l'aimant
    
    // Dimension du Bloc aimant principal
    // le carré des aimants et de la matière 
    Tbloc=4*Paimant + Epbords;
    
    // hauteur du bloc
    Hbloc = Haimant + epaissocl;

    // trous de fixation éventuels corrigé de l'écart
    Dfix= Dfixtheo +e2;

    
    // diamètre de l'axe corrigé de l'écart de précision
    Axe = diamaxe + (2*ecart);
    



/*  2. MODELISATIONS */

/*  LOGEMENT DES AIMANTS*/
        
        // Logement d'un aimant
            module logaimant() {
                color("pink")
                translate([-(Laimant/2),Paimant,0])
                cube(size = [Laimant,Paimant,Haimant], center = false);
                }
        // Logements des aimants 
            module logXaimants(i) {
                translate([0,0,epaissocl])
                for(i = [0 : (360/Naimants) : 360])
                rotate(i) 
                logaimant();
                }
        
/*  BLOC DE MAINTIENT DES AIMANTS */
        module BlocPartie() {
                color("grey")
                translate([0,0,(Hbloc)/2])
                cube(size = [Tbloc,Tbloc,Hbloc], center = true);
                }
        module Coin() {
                color("yellow")
                translate([(Laimant/2)+Epbords,(Laimant/2)+Epbords,0])
                cube(size = [Lcoin,Pcoin,Hcoin], center = false);
                }
        module DesCoins() {
                translate([0,0,0])
                for(i = [0 : (360/Naimants) : 360])
                rotate(i) 
                Coin();
                }
        
        module TrousFix() {
                for(a = [-10 : 20 : 10])
                for(b = [10 : -20 : -10])
                translate([a,b,0])
                cylinder(h = (Infini), d =Dfix, center = true);
                }
        module Oeil() {
                translate([0,Paimant*2,((Laimant-6)/2)+epaissocl+3])
                rotate([90, 0, 0])                
                cylinder(h = (Paimant), d =(Laimant-6), center = true);
                }
        module Oeils() {
                for(a = [0 : (360/Naimants) : 360])
                rotate(a)
                Oeil();
                }      

        module TourRond() {
                difference() {
                    translate([0,0,(Hbloc/2)])
                    cylinder(h = (Hbloc), r =(Rcour), center = true);
//                    cylinder(h = (Hbloc), r =(Rcour-2), center = true);
                    }
                
                }
                
            
        module axe() {
                color("pink")
                translate([0,0,0])
                cylinder(h = (Infini), d =Axe, center = true);               
            }

/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){

        TourRond();
        }

        #logXaimants();
        #axe();
        #TrousFix();
        #Oeils();
}

/* FIN DE DOCUMENT */