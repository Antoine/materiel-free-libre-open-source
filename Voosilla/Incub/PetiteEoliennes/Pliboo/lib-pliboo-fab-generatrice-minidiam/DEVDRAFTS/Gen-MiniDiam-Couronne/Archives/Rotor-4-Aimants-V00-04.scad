/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un bloc rotor pour loger 4 aimants
*/

/* #########################
*  NOTE DE VERSION
*  #########################
*  Version 00-04, suite de 00-03:
*  Ajout d'un connections alvéolaires centrales
*  sur le dessus et sur le dessous
*/ 
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/*  1.1 PARAMETRES REGLABLES */

    include <Rotor-4-Aimants-Donnees-Communes-V00-01.scad>

/*  2. MODELISATIONS */

/*  LOGEMENT DES AIMANTS*/
        
        // Logement d'un aimant
            module logaimant() {
                color("pink")
                translate([-(Laimant/2),Paimant,epaissocl])
                cube(size = [Laimant,Paimant,Hbloc], center = false);
                }
        // Logements des aimants 
            module logXaimants(i) {
                translate([0,0,epaissocl])
                for(i = [0 : (360/Naimants) : 360])
                rotate(i) 
                logaimant();
                }
        
/*  BLOC DE MAINTIENT DES AIMANTS */
        module BlocPartie() {
                color("grey")
                translate([0,0,(Hbloc)/2])
                cube(size = [Tbloc,Tbloc,Hbloc], center = true);
                }
        module Coin() {
                color("yellow")
                translate([(Laimant/2)+Epbords,(Laimant/2)+Epbords,0])
                cube(size = [Lcoin,Pcoin,Hcoin], center = false);
                }
        module DesCoins() {
                translate([0,0,0])
                for(i = [0 : (360/Naimants) : 360])
                rotate(i) 
                Coin();
                }
        
        module TrousFix() {
                for(a = [-10 : 20 : 10])
                for(b = [10 : -20 : -10])
                translate([a,b,0])
                cylinder(h = (Infini), d =Dfix, center = true);
                }
        module Oeil() {
                translate([0,Paimant*2,((Laimant-6)/2)+epaissocl+3])
                rotate([90, 0, 0])                
                cylinder(h = (Paimant), d =(Laimant-6), center = true);
                }
        module Oeils() {
                for(a = [0 : (360/Naimants) : 360])
                rotate(a)
                Oeil();
                }      

        module TourRond() {
                difference() {
                    translate([0,0,(Hbloc/2)])
                    cylinder(h = (Hbloc), r =(DEXTrotor), center = true);
//                    cylinder(h = (Hbloc), r =(DEXTrotor-2), center = true);
                    }
                
                }
                
            
        module axe() {
                color("pink")
                translate([0,0,0])
                cylinder(h = (Infini), d =Axe, center = true);               
            }
            
    // L'EXCAVATION POLYGONALE
        module abeille() {
                color("pink")
                translate([0,0,0])
                linear_extrude(height = epaissocl, center = true, convexity = 10, twist = 0) 
                circle(AlveoleGranda,$fn=6);
            }
        module abeilles() {
            union(){
                translate([0,0,(epaissocl/2)+Hbloc-epaissocl]) abeille();                }
            }
/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){

        TourRond();
        }

        #logXaimants();
        #axe();
        #TrousFix();
        #Oeils();
//        abeilles();
}

/* FIN DE DOCUMENT */