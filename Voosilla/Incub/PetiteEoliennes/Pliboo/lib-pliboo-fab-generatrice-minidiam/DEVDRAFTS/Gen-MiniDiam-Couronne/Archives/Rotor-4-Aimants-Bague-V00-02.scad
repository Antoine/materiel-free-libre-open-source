/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un bloc rotor pour loger 4 aimants
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/*  INCLUSION, IMPORTATION DE DONNÉES*/
    
    // Inclusion de fichiers SCAD
    include <Rotor-4-Aimants-Donnees-Communes-V00-01.scad>    

/*  PARAMETRES SPÉCIFIQUES */

    // ÉPAISSEUR
        Epaisseur = 3;
    
    // COLLIER DE SERRAGE
        DiamCollierExt = 15; // Diam externe du collier
        LfentesCollier = 3; // Largeur des Fentes du Collier
        HCollier = 10; // Hauteur du Collier

/*  2. MODELISATIONS */
              
    // L'AXE CENTRAL        
        module axe() {
                color("pink")
                translate([0,0,0])
                cylinder(h = (Infini), d =Axe, center = true);               
            }
        
        module tubecollier() {
                difference() {
                    union() {
                        color("violet")
                        translate([0,0,(HCollier/2)])
                        cylinder(h = HCollier, d =DiamCollierExt, center = true);
                        
                        color("violet")
                        translate([0,0,(1/2)])
                        cylinder(h = 1, d =DiamCollierExt+2, center = true);
                        
                        color("violet")
                        translate([0,0,(1/2)+HCollier])
                        cylinder(h = 1, d =DiamCollierExt+2, center = true);     
                    }
                    color("grey")
                    translate([0,10,0])
                    #cube(size = [LfentesCollier,DiamCollierExt,Infini], center = true);
                }
              }  
            
/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        tubecollier();
        }
        #axe();
}

/* FIN DE DOCUMENT */