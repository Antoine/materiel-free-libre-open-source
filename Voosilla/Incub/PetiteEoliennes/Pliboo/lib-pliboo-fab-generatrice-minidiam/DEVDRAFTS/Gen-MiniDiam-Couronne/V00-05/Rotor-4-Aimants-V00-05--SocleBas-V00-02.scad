/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un bloc rotor pour loger 4 aimants
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/*  INCLUSION, IMPORTATION DE DONNÉES*/
    
    // Inclusion de fichiers SCAD
    include <Rotor-4-Aimants-Donnees-Communes-V00-01.scad>    

/*  PARAMETRES SPÉCIFIQUES */

    // ÉPAISSEUR
        Epaisseur = 3;

/*  2. MODELISATIONS */
        
    // TROUS DE FIXATIONS
        module TrousFix() {
                color("blue")
                for(a = [-10 : 20 : 10])
                for(b = [10 : -20 : -10])
                translate([a,b,(Epaisseur/2)+Epaisseur])
                cylinder(h = (Epaisseur), d =Dfix-e2, center = true);
                }
                
    // PLATEAU DU CHAPEAU
        module TourRond() {
                translate([0,0,(Epaisseur/2)])
                cylinder(h = (Epaisseur), r =(20), center = true);    
                }
                
    // L'AXE CENTRAL        
        module axe() {
                color("pink")
                translate([0,0,0])
                cylinder(h = (Infini), d =Axe, center = true);               
            }
    
    // L'EXCAVATION POLYGONALE
        module abeille() {
                color("pink")
                translate([0,0,0])
                linear_extrude(height = Infini, center = true, convexity = 10, twist = 0) 
                circle(Abeille,$fn=6);
            }
     
/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        
        TourRond();
        TrousFix();
        }
        axe();
        abeille();
        
}

/* FIN DE DOCUMENT */