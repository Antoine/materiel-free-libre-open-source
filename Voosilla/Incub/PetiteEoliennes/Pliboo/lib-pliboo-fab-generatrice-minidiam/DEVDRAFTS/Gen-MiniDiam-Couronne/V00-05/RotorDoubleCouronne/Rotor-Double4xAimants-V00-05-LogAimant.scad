/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un bloc rotor pour loger 4 aimants
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/*  INCLUSION, IMPORTATION DE DONNÉES*/
    
    // Inclusion de fichiers SCAD
    include <Rotor-Double4xAimants-V00-05-Commons.scad>;

/*  PARAMETRES SPÉCIFIQUES */

/*  2. MODELISATIONS */


/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

        difference(){
            union(){
//                bras1();
//                braschap();
        //        aimant();
                boite();
                }
        }



/* FIN DE DOCUMENT */