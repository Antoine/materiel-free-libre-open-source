/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un bloc rotor pour loger 4 aimants
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/*  INCLUSION, IMPORTATION DE DONNÉES*/
    
    // Inclusion de fichiers SCAD

/*  PARAMETRES SPÉCIFIQUES */

    // ÉPAISSEUR
        ep2= 2; // épaisseur de la pièce
    
    // COLLIER DE SERRAGE

/*  2. MODELISATIONS */
        module bras1() {
                color("Aqua",1)
                linear_extrude(height = ep2, center = false, convexity = 10)
                import(file = "lib-generatrice-minidiam-doublecourronne-00-01-001.dxf", layer = "bras");
            }
            
        module braschap() {
            translate([0,0,ep2])
            color("Fuchsia",1)
            linear_extrude(height = ep2, center = false, convexity = 10)
            import(file = "lib-generatrice-minidiam-doublecourronne-00-01-001.dxf", layer = "braschap");
            
            translate([0,0,ep2])
            color("Fuchsia",1)
            linear_extrude(height = ep2*3, center = false, convexity = 10)
            import(file = "lib-generatrice-minidiam-doublecourronne-00-01-001.dxf", layer = "griffe");
            }

       module aimant() {
            translate([0,0,-36])
            color("grey",1)
            linear_extrude(height = 26, center = false, convexity = 10)
            import(file = "lib-generatrice-minidiam-doublecourronne-00-01-001.dxf", layer = "aimant");
            }            
       module boite() {
           difference() {
            translate([0,0,-39])
            color("Lavender",1)
            linear_extrude(height = 39, center = false, convexity = 10)
            import(file = "lib-generatrice-minidiam-doublecourronne-00-01-001.dxf", layer = "boitier");
               
            translate([0,0,-36])
            color("Ivory",1)
            linear_extrude(height = 36, center = false, convexity = 10)
            import(file = "lib-generatrice-minidiam-doublecourronne-00-01-001.dxf", layer = "EmportePiece");
               }
            }            
/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        bras1();
        braschap();
//        aimant();
        boite();
        }
}

/* FIN DE DOCUMENT */