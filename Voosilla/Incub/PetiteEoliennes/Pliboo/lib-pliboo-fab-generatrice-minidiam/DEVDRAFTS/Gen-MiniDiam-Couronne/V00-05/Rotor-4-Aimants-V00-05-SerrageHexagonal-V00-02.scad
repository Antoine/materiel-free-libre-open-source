/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un bloc rotor pour loger 4 aimants
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/*  INCLUSION, IMPORTATION DE DONNÉES*/
    
    // Inclusion de fichiers SCAD
    include <Rotor-4-Aimants-V00-05-Donnees-Communes-V01.scad>    

/*  PARAMETRES SPÉCIFIQUES */

    // ÉPAISSEUR
        Epaisseur = 3;
    
    // COLLIER DE SERRAGE
        LfentesCollier = 3; // Largeur des Fentes du Collier
        HCollier = 10; // Hauteur du Collier
        EpDebord = 2; // Épaisseur du débord
        DiamPlastiq = 11; //Diamètre extérieur d'un tube en plastique  de 8mm (Mettre la valeur 'Axe' si aucun tube plastique)
        
    // CALCULS SPÉCIFIQUES
        EpaisseurTubPlast = DiamPlastiq - Axe; // Épaisseur tube plastique
        DiamTrou = Axe + (EpaisseurTubPlast*2); // Diamètre du trou autour du tube en plastique
        DiamCollierExt = DiamTrou + 6; // Diam externe du collier
        
/*  2. MODELISATIONS */

    // PIED HEXAGONAL
            module abeille() {
                color("pink")
                translate([0,0,Epaisseur/2])
                linear_extrude(height = Epaisseur, center = true, convexity = 10, twist = 0) 
                circle(Abeille-(e2/2),$fn=6);
                
                color("violet")
                translate([0,0,(1/2)+Epaisseur])
                cylinder(h = EpDebord, r=Abeille+2, center = true);     
            }
              
    // L'AXE CENTRAL        
        module axe() {
                color("pink")
                translate([0,0,0])
                cylinder(h = (Infini), d =DiamTrou, center = true);               
            }
    // LE CYLINDRE FENDU DE SERRAGE
        module tubecollier() {
                        color("violet")
                        translate([0,0,((HCollier+EpDebord)/2)+Epaisseur])
                        cylinder(h = HCollier, d =DiamCollierExt+e2, center = true);
                                                
//                        color("violet")
//                        translate([0,0,(1/2)+HCollier+Epaisseur])
//                        cylinder(h = 1, d =DiamCollierExt+2, center = true);     
                    }
                    
        module fente() {
                    color("grey")
                    translate([0,DiamCollierExt/2,0])
                    #cube(size = [LfentesCollier,DiamCollierExt,Infini], center = true);
           }
        module fentes() {
                    color("grey")
                    for(a = [0 : 360/3 : 360])
                    rotate(a)
                    fente();     
        }
        module collier() {
                difference(){
                    tubecollier();
                    fentes();
                    }
            }
        
/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        abeille();
        collier();
        }
        #axe();
}

/* FIN DE DOCUMENT */