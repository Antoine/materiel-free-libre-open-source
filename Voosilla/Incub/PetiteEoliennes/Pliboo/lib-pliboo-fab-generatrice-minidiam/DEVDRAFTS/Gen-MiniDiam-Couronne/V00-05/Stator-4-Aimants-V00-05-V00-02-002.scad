/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un bloc stator
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/*  IMPORTATIONS DEPUIS D'AUTRES FICHIERS */
    
    // PARAMETRES APPELES DEPUIS D'AUTRES FICHIERS SCAD
    // INCLUSION 
    include <Rotor-4-Aimants-V00-05-Donnees-Communes-V01.scad>;
    
/*  PARAMETRES SP"CIFIQUES */
    Nbob = 3; // Nombre de bobines
    Hint = Nbob+45;
    Halveol = 20;
    EpH = 2; // épaisseur des parties horizontales
    Dcoeur = 20; // diamètre du socle au coeur
    HpieceHT = 65; // Hauteur du dispositif hors tout
    EnF = 1.5; // Entrefer entre le rotor et le stator
    EpP = 1; // Épaisseur des paroies
    EpB = 10; // Épaisseur de la bobine
    
    D0 = DEXTrotor; // Diamètre de base = diamètre du rotor
    D1 = DEXTrotor + EnF; // Diamètre de surface intérieure de la paroie intérieure
    D2 = D1 + EpP; // Diamètre de la surface extérieure de la paroie intérieure
    D3 = D2 + EpB; // Diamètre de la surface intérieure de la paroie extérieure
    D4 = D3 + EpP; // Diamètre de la surface extérieure de la paroie extérieure
    
/*  DEFINITIONS DE MODULES */

    // SOCLE : UN DISQUE
        module socleur() {
                color("yellowgreen")
                translate([0,0,EpH/2])
                cylinder(h=EpH,r=Dcoeur,center=true);
            }
    // 4 TROUS SUR UNE GRILLE (10n,10n)
        module trousage() {
            for(x = [-10 : 20 : 10]) // x=(-10,10)
            for(y = [-10 : 20 : 10]) // y=(-10,10)
                translate([x,y,0])
                color("grey")
                cylinder(h =Infini, d = Dfix, center = true);  
            }
    // PIEDS DE SUPPORT
            // PIED
                module pied() {
                    difference() {
                        translate([35,0,EpH/2]) 
                        color("violet")              
                        cube(size = [40,10,EpH], center = true);
                                      
                        translate([49,0,0])
                        color("salmon")
                        cylinder(h =Infini, d = Dfix, center = true);  
                    }
                }
            // 6 PIEDS
                module pieds() {
                    for(p = [0 : 60 : 300])
                        rotate(a=p, v=[0,0,1])
                        pied();
                    }

        // PAROIE EXTERNE
        module doubleparext() {
            difference() {
                // PAROIE EXTERNE = UN CYLINDRE PLEIN À EXCAVER
                    translate([0,0,HpieceHT/2])
                    color("salmon")
                    cylinder(h =HpieceHT, r = 45, center = true);  
                // PAROIE EXTERNE = UN CYLINDRE QUI EXCAVE
                translate([0,0,HpieceHT/2+3])
                color("black")
                cylinder(h =Infini, r = 44, center = true);
            }
            }
    // PAROIE INTERNE
        module doubleparint() {
            difference() {
                // PAROIE INTERNE = UN CYLINDRE PLEIN À EXCAVER
                    translate([0,0,65/2])
                    color("salmon")
                    cylinder(h =65, r = 33.5, center = true);  
                // PAROIE INTERNE = UN CYLINDRE QUI EXCAVE
                translate([0,0,65/2+3])
                color("black")
                cylinder(h =600, r = 32.5, center = true);
            }
            }
    // TROU DE PASSAGE DE L'AXE
        module axiolage() {
                translate([0,0,0])
                color("salmon")
                cylinder(h =500, r = 8, center = true);  
            }

    // SEPARATEURS POUR BOBINES
        module sepa() {
                translate([32.5,0,0])
                rotate(a=-90, v=[0,0,1]) 
                color("salmon")
                cube(size = [1,12,65], center = false);
            }
        module sepas() {
            for(y = [0 : 60 : 360-60])
                rotate(a=y, v=[0,0,1]) 
                sepa();
            }
            
    // SUPPORTS POUR BOBINES
        module sup() {
                translate([32.5,0,0])
                rotate(a=-90, v=[0,0,1]) 
                color("blue")
                cube(size = [1,12,20], center = false);
            }
        module sups() {
            for(m = [30 : 60 : 360-30])
                rotate(a=m, v=[0,0,1]) 
                sup();
            }

            
    // EXCAVATION DE MATIÈRE: LES PAROIES "INUTILES"
        module antimat() {
                translate([27,1,3])
                rotate(a=-90, v=[0,0,1]) 
                color("grey")
                cube(size = [1,20,70], center = false);               
            }
        module antimats() {
            for(m = [0 : 1 : 57])
                rotate(a=m, v=[0,0,1])             
                antimat();
            }
        module antimatx() {
            for(g = [0 : 120 : 240])
                rotate(a=g, v=[0,0,1])             
                antimats();
            }
    // ALVEOLES AERANTES REDUCTRICE DE MATIERE
        module alveoliae() {
                color("greenyellow")
                linear_extrude(height = Halveol, center = true, convexity = 10) 
                translate([0,0,0])
                circle(3,$fn=6);
            }
        module alveolia() {
                translate([0,(Halveol/2)+30,10])
                rotate(a=90, v=[1,0,0])
                alveoliae();
        }
        module alveoliaH1() {
                for(al = [0 : 20 : 28])
                    rotate (a=al, v=[0,0,1])
                    alveolia();
            }
        module alveoliaH2() {
                for(al = [0 : 20 : 10])
                    rotate (a=al, v=[0,0,1])
                    translate([0,0,7])
                    alveolia();
            }
        module alveoliaH1H2() {
                union() {
                    alveoliaH1();
                    alveoliaH2();
                    }
            }
        module alveoliaV() {
                for(s = [0 : 8 : 50])
                    translate([0,0,s])
                    alveolia();
            }
        module alveoliaVx1() {
            alveoliaV();
            translate([0,0,-5]) rotate (a=11, v=[0,0,1]) alveoliaV();
            translate([0,0,-5]) rotate (a=-11, v=[0,0,1]) alveoliaV();
            translate([0,0,0]) rotate (a=22, v=[0,0,1]) alveoliaV();
            translate([0,0,0]) rotate (a=-22, v=[0,0,1]) alveoliaV();
            }
        module alveoloaF() {
            for(q = [0 : 120 : 240])
                    rotate (a=q, v=[0,0,1])
                    alveoliaVx1();
            }
        module paroiesalveoles() {
                difference() {
                    union() {
                        doubleparext();
                        doubleparint();
                        }
                    alveoloaF();
                    }
            }
/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        socleur();
//        doubleparext();
//        doubleparint();
        paroiesalveoles();
//        sepas();
        pieds();
        sups();

//        portages();
//        soclage();
//        logisroul();
//        petons();
        }
//        roulemapoule();
        trousage();
//        axiolage();
//        antimatx();
        alveoloaF();
}
//        sups();

/* FIN DE DOCUMENT */