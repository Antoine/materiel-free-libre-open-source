/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Depuis Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Paramètres de dimensionnement commun
*  à plusieurs fichiers SCAD
*  pour l'impression 3D d'un mini-rotor de 4 aimants
*  façon ABA
*/

/* PARAMETRES COMMUNS AUX FICHIERS
*  GEN-4-AIMANTS ABA
*/
    // ### PARAMÈTRES À SAISIR ###
    // ECARTS DE PRÉCISION
    Precision = 0.3; // écart de précision pour trous et paroies internes

    // INFINI DES ÉLÉMENTS TRAVERSANTS
    Infini=200;
    
    // NOMBRE ET TAILLE DES AIMANTS UTILISÉS
    // parallépipédiques rectangles
    // posé debout à la vertical
    // Hauteur = plus grande arête
    // Largeur = seconde plus grande arête
    // Profondeur = plus petite arête
    HAimant=13; // Hauteur de l'aimant
    LAimant=25; // Largeur de l'aimant
    PAimant=25; // Profondeur de l'aimant
    Naimants=4; // Nbre d'aimants

    // NOTIONS D'ÉPAISSEUR DES PAROIS ET SOCLES
    EpSocl = 3; // épaisseur du socle
    EpBords = 1; // Epaisseur des Bords

    // DIAMETRE DE L'AXE
    DiamAxe = 8;
        
    // RAYON DU CERCLE CONTENANT LA FIXATION
    // HEXAGONALE
    Abeille = 10; //
    
    // HAUTEURS ET EPAISSEURS DANS LE FLUX DE L'AXE
    // pour y voir clair dans la dimension "hors tout"
    HFluxCouv = 3; // Hauteur d'épaisseur des couvercles
    HFluxRebRoul = 4; // Hauteur du rebord support du roulement
    HFluxRoul = 7; // Épaisseur du roulement
    HFluxConnect = 3; // Hauteur de la bague de connexion rotor/stator
    HFluxBague = 12; // Hauteur de zone de bague de serrage
    HFluxDebord = 3; // Épaisseur de la rondelle de débord de la bague serrage
    HFluxEspAimant = 10; // Hauteur de l'espace vide au dessus des aimants

    HFluxSocle = 3; // épaisseur du socle sous l'aimant
    HFluxJeu = 3; // écart pouvant être comblé par des rondelles

    
    // #### PARAMETRES CALCULÉS ####
    
    // ECARTS DE PRÉCISION
    Px2=2*Precision; // Un écart de double précision
    
    // LOGEMENT DE L'AIMANT
    // la taille de l'aimant + écart pour faire passer l'aimant
    HLogAimant=HAimant+Px2; // Hauteur du logement de l'aimant
    LLogAimant=LAimant+Px2; // Largeur du logement  de l'aimant
    PLogAimant=PAimant+Px2; // Profondeur du logement de l'aimant

    // DIAMETRE DE L'AXE
    Axe = DiamAxe + Px2; // diamètre de l'axe corrigé de l'écart de précision
    
    // DONNEES DU BLOC ROTOR

