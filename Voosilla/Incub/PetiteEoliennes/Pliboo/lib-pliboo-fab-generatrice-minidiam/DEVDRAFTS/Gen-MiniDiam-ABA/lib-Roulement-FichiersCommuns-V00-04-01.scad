/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Les données communes aux fichiers
*  => des paramètres communs pour les pièces de roulement
*/
    

/* #########################
   1. PARAMÈTRES 
   #########################
*/

/*  PARAMETRES COMMUNS */

    // PARAMÈTRES À SAISIR
        Precision = 0.2; // écart de précision versus impression 3D
        Infini = 500; // dimension d etrouage infini
        DiamFixTheo = 5; // diamètre théorique des trous de fixation
        DiamRoulTheo = 22; // diamètre extérieur théorique du roulement
        DiamTrouCentral = 16; // diamètre du trou central
        HautRoul = 7; // Hauteur du roulement
        HautSocle = 3; // Épaisseur du socle
        EpaisParoie = 3; // Épaisseur des paroies verticales
        HInterRoul = 20; // Hauteur de la pièce entre deux roulements
        
    // PARAMÈTRES CALCULÉS
        P2 = 2*Precision; //
        DiamFix = DiamFixTheo+P2; // diamètre du trou des fixations
        DiamRoul = DiamRoulTheo + P2; // diamètre du trou pour le roulement
        ParoieExtRoul = DiamRoul + EpaisParoie; // diamètre extérieur la paroie du logement du roulement
        HautHT = HautRoul + HautSocle;
    
/* FIN DE DOCUMENT */