/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARAMETRES REGLABLES */
// Un cylindre avec un fond et un rebord extérieur en haut
    haut = 3; //prodondeur du trou
    efond = 0.2; //épaisseur du fond
    rint = 10; //rayon intérieur du trou du cylindre
    rext = rint + 3; //rayon du bord extérieur du cylindre 
    ebord = 3; // épaisseur du rebord du haut
    rbord = rext + 4; // rayon ext du rebord du haut
    rvis = 2.5; //rayon passage vis
    epa = 3; // épaisseur de la bague
    fent = 3; // taille de la fente
    correctR = 0.35; //corrige le rayon interne
    
/* TRUCS */
    module corpsprincipal() {
        cylinder(h = haut + efond + ebord , r = rext, center = false);
    }
    
    module troumilieu() {
        translate([0,0,efond]) #cylinder(h = 50, r = rint + correctR, center = false);
    }
    
    module rebord() {
        translate([0,0,haut]) cylinder(h = ebord , r = rbord, center = false);
    }
    
    module fente() {
        translate([0,8,0]) cylinder(h = 10, r = dext, center = true);
    }
    
    module machoire() {
        translate([0,0,5]) translate([-fent,8,0]) cube(size = [epa,18,20], center = true);
        translate([0,0,5]) translate([fent,8,0]) cube(size = [epa,18,20], center = true);
    }
    
    module tubeext() {
        cylinder(h = 10, r = dext, center = true);
    }
    
    module trouH() {
        #translate([0,11,10]) rotate([0,90,0]) cylinder(h = 40, r = dvis, center = true);
    }
    
    module apendice() {
        translate([0,0,10]) cylinder(h = 30, r = dint + 3, center = true);
    }

//module troumilieu()
//{#translate([0,0,0]) cylinder(h = 80, r = dint, center = true);}

module baseblocfixeur() {
    rotate([0,0,45]) translate([0,-10,0]) cube(size = [10,20,10], center = true);
    }
module troublocfixeur() {
    #rotate([0,0,0]) translate([10,-10,0]) cylinder(h = 15, r = 2.9, center = true);
    }
module blocfixeur() {
    difference(){
        union(){
            baseblocfixeur();
            }
            troublocfixeur();
            }
    }
/* #########################
   2.  MISE EN FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        corpsprincipal();
        rebord();
	}
		#troumilieu();
        
}

/* FIN DE DOCUMENT */