/* lib-pliboo-fab-PlotPrecision
*  version V15-05 devDraft-0 révision 00
*  ------------------------------
*  Guide pour axe avec ailettes
*  --
*  Mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* 1.1 LE TUBE DE L'ETE */

module tube()
		{difference()
				{union()
					{cylinder(h = 16, r = 4, center = false);
					}/*fin de fonction union*/
				 #cylinder(h = 80, r = 2.85, center = true);
				}/* fin de fonction difference */	
		}/* fin de module */

  
        
        /* #########################
   2.  MISE EN FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

tube();
/* FIN DE DOCUMENT */