/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un petit coffret pour y placer un aimant
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARAMETRES REGLABLES */
ecart = 0.3; //écart de précision versus théorie

/* Saisie Paramètres Largeurs */
Ldiam = 25; //diamètre de l'aimant
Ltrou = 28; //diamètre du trou dans le bois

/* Saisie Paramètres Hauteurs */
Hbois = 10; //épaisseur du bois
Hdiam = 3; //épaisseur de l'aimant
Hcyl = 15; //hauteur du cylindre logeur
Hcour = 1; //épaisseur des disques horizontaux
Htrou = 100; //trou maximal
EPfonds = 1; //épaisseur des fonds
EPbords = 0.7; //épaisseur des bords
EPparoi = 1; //épaisseur de la paroi du coinceur
Ldebord=1.5; //largeur du débord

/* Paramètres calculés */
Ldiam2 = Ldiam + (2*ecart); // diam trou logement de l'aimant
Ldext = Ltrou - (2*ecart); //diam ext cylindre logeur
Lcoext = Ldiam2-(2*ecart); //diam ext cylindre coinceur
Lcoint = Lcoext - (2*EPparoi);//disque intérieur du coinceur
Hcylco = Hcyl - Hdiam - ecart; //hauteur du cylindre coinceur


epfonds = 0.7; //épaisseur des fonds
epbords = 0.7; //épaisseur des bords
epbois = 10; //épaisseur de la plaque support
daim = 25 +ecart; //diamètre de l'aimant



/* MODELISATIONS */
    /* Partie 1: le réceptacle de l'aimant*/
    /* Un cylindre excavé */
    module coffret() {
        difference() {
            union() {
                color("red")
                translate([0,0,((Hcyl)/2)])
                cylinder(h = (Hcyl), d =Ldext, center = true);
                
                color("orange")
                translate([0,0,((EPfonds)/2)+Hbois])
                cylinder(h = (EPfonds), r =(Ldext/2)+Ldebord, center = true);
                
                }
            color("pink") 
            translate([0,0,((Htrou)/2)+EPfonds]) 
            cylinder(h = Htrou, d = (Ldiam2), center = true);
            
            color("violet") 
            translate([0,0,EPfonds/2]) 
            cylinder(h = Htrou, d=5, center = true);
            }
        }
        
    /* Partie 2: le boitier bloqueur*/
    /* Un cylindre excavé avec un chapeau*/
    module boitier() {
        difference() {
            union() {
                color("blue")
                translate([0,0,((Hcylco)/2)])
                cylinder(h = (Hcylco), d = Ldiam2-(ecart), center = true);
                
                color("green")
                translate([0,0,((EPfonds)/2)])
                cylinder(h = EPfonds, d = Ldiam2+(2*Ldebord), center = true);
                               
                }
                
            color("grey") 
            translate([0,0,((Htrou)/2)+EPfonds]) 
            cylinder(h = Htrou, d = Lcoint, center = true);
                
            color("violet") 
            translate([0,0,epfonds/2]) 
            cylinder(h = Htrou, d =5, center = true);
            }
        }

    module pousseur() {
        color("violet") 
        translate([0,0,epfonds/2]) 
        cylinder(h = epfonds, d =5, center = true);
        }
/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        //translate([40,0,0]) coffret();
        boitier();
        }
}

/* FIN DE DOCUMENT */