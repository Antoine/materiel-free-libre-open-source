/* lib-pliboo-fab-logemonroulement
*  version V15-05 devDraft-0 révision 00
*  ------------------------------
*  Guide pour axe avec ailettes
*  --
*  Mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARTIE BASSE */
    module trouroulement() {
        #translate([0,0,3])  rotate([0,0,0]) cylinder(h=7, r=11.25, center = false);
    }
 
/* UN SOCLE TROUE PAR 4 TROUS 10x,10y */
    module trou() 
        {#translate([0,0,-5]) cylinder(h = 70, r = 2.75, center = false);}
    module trous() 
    {
    translate([10,10,0]) trou();
    translate([-10,10,0]) trou();
    translate([-10,-10,0]) trou();
    translate([10,-10,0]) trou();
    }
    module socle() 
    {translate([0,0,0]) cylinder(h =10, r = 20, center = false);}
       
    module socleatrous() {
        difference(){
            union(){
                socle();
            }
            #trous();
        }
    }
    
    module troudumilieu() 
    {#translate([0,0,0]) cylinder(h = 20, r = 8, center = false);}
    
/* #########################
   2.  MISE EN FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */
    difference(){
        union(){
             socleatrous();
        }
        #troudumilieu();
        #trouroulement();
    }
   
    
/* FIN DE DOCUMENT */