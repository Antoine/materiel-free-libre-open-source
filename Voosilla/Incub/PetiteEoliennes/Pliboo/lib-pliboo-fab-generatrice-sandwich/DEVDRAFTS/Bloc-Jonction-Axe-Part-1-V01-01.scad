/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. DE QUOI S AGIT-IL
   #########################
*  Un bloc de jonction avec un axe
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/*  1.1 PARAMETRES REGLABLES */

/*  1.1.1 Paramètres d'écart de précision 
*   en fonction de l'imprimante utilisée */

    //écart de précision pour un rayon ou une surface versus théorie
    ecart = 0.3; 

/*  1.1.2 Paramètres à saisir */

    // diamètre de l'axe
    diamaxe = 8;
    
    // hauteur du collier de serrage
    hautcoll = 8;
    
    // hauteur du cintrable sur l'axe
    hautcintr = 25;
    
    // hauteur des équerres
    hautequer = 6;
    
    // épaisseur du socle
    epaissocl = 5;
    
    // largeur maximale de l'écrou 
    largecrou = 8;
    
    // diamètre intérieur de l'écrou
    diamintecrou = 5;
    
    // épaisseur des paroies coinceurs écrou
    epaisparecrou = 2;
    
    // hauteur des écrous
    hautecrou = 5;
    
    // épaisseur du coeur sur l'axe
    tuyau = 3;
    
    // hauteur des plaques en étoile
    hautplaq = 10;

/*  1.1.3. Paramètres calculés */
    
    // dimension ext du cube logeur de l'écrou
    Extecrou = largecrou + epaisparecrou;
    
    // dimension intérieur du cube logeur de l'écrou
    Intecrou = largecrou + (2*ecart);
    
    // diamètre de l'axe corrigé de l'écart de précision
    Axe = diamaxe + (2*ecart);
    
    // diamètre écrou corrigé de l'écart
    Decrou = diamintecrou + (2*ecart);



/*  2. MODELISATIONS */

/*  2.2 Cercle de maintient */
        module couronne() {
            difference(){
                union(){
                    color("green")
                    translate([0,0,epaissocl/2])
                    cylinder(h = epaissocl, r =45, center = true);
                    }
                color("blue")
                translate([0,0,epaissocl/2])
                cylinder(h = epaissocl, r =40, center = true);
                }
            }


/*  2.3 Bras */         
            module brasuno() {
                translate([0,25,epaissocl/2]) 
                cube(size = [14,50,epaissocl], center = true);
                }
            module brasx() {
                union() {
                    for(i = [0 : 45 : 360])
                    rotate(i) brasuno();
                    }
                }
/*  2.1 Logement des écrous
*/
    // Logement des écrous
    // Modèle pour le logement d'un écrou troué
    module logecrou() {
            color("yellow")
            // un ecrou à faces, utilisation de FOR et d'un demi carre d'ecrou 
            for(i = [0 : 120 : 360]) 
            rotate(i) 
            cube(size = [Intecrou,(Intecrou/2),hautecrou], center = true);
            cylinder(h = (100), d =diamintecrou+(2*ecart), center = true);               
        }
    
    // Huit logement d'écrous répartis sur une grille n10,n10
    module ecrous() {
        union(){
            
            translate([40,0,0]) logecrou();
            translate([30,30,0]) logecrou();
            translate([0,40,0]) logecrou();
            translate([-30,30,0]) logecrou();
            translate([-40,0,0]) logecrou();
            translate([-30,-30,0]) logecrou();
            translate([0,-40,0]) logecrou();
            translate([30,-30,0]) logecrou();
            }
        } 
        
        
        
/*  2.4 Cylindre du milieu */
             
        module tuyau() {
                    color("red")
                    translate([0,0,hautcintr/2])
                    cylinder(h = (hautcintr), d =Axe+(2*tuyau), center = true); 
                    }
            
        module axe() {
                color("pink")
                translate([0,0,hautcintr/2])
                cylinder(h = (hautcintr), d =Axe, center = true);               
            }

        module sections() {
                color("grey")
                translate([0,0,hautcintr/2])
                for(i = [0 : 60 : 360]) 
                rotate(i) 
                cube(size = [2,20,hautcintr], center = true);
            }
        
        module crabe() {
                difference() {
                    union(){
                        tuyau();
                        }
                    #sections();
                    }
            }    
            
//  2.5 ailettes en triangle
        module() uneail() {
            polygon (points=[[0,0],[50,0],[0,10]])
            }
            
/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        couronne();
        brasx();
        crabe();
        }
        translate([0,0,(hautecrou/2)+1]) ecrous();
        axe();
}

/* FIN DE DOCUMENT */