/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARAMETRES REGLABLES */
// Diamètre du tube central
    dint = 4.35; //diam exterieur
    dext = dint + 3; //diam intérieur
    dvis = 2.5; //diam passage vis
    epa = 3; // épaisseur de la bague
    fent = 3; // taille de la fente
    
/* UNE BAGUE A LA TIGE  */
        
    module fente() {
        translate([0,8,0]) #cube(size = [fent,18,20], center = true);
    }
    
    module machoire() {
        translate([-fent,8,0]) cube(size = [epa,18,10], center = true);
        translate([fent,8,0]) cube(size = [epa,18,10], center = true);
    }
    
    module tubeext() {
        cylinder(h = 10, r = dext, center = true);
    }
    
    module trouH() {
        #translate([0,11,0]) rotate([0,90,0]) cylinder(h = 40, r = dvis, center = true);
    }
    

module troumilieu()
{#translate([0,0,0]) cylinder(h = 40, r = dint, center = true);}


/* #########################
   2.  MISE EN FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        machoire();
        tubeext();

	}
		troumilieu();
        trouH();
        fente();
}

/* FIN DE DOCUMENT */