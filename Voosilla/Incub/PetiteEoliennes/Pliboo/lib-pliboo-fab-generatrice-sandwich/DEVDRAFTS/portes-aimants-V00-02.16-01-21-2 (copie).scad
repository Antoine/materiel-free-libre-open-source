/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. ce quoi s'agit-il ? 
   #########################
*  Un coffret pour y placer un aimant
*  en forme de parallépidpède rectangle
*  de dimension L25,4 x P25,4 x H12,7 mm
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARAMETRES REGLABLES */
    // Paramètres de l'aimant:
    Haimant = 12.7; // Hauteur de l'aimant
    Esocle = 1; // Épaisseur du socle
    Edebord = 1; // Épaisseur débord
    Adjust = 0.5; // Marge d'ajustage
    Hparois = Haimant + Esocle + Adjust + Edebord; // Hauteur des paroies, fond inclus
    Hx = 100; // hauteur de trouée
    Hbois = 10; // épaisseur du bois
    
    La = 25.4; // largeur de l'aimant en mm
    Pa = La; // profondeur de l'aimant en mm
    Ha = 12.7+1; // hauteur de l'aimant en mm

    // Paramètres des parois du coffrage
    Hc = Ha + 0; // hauteur des paroies
    d1 = 2; // diamètre des boudins d'aspérité
    ep = 1; // épaisseur paroi
    epf = 0.7; // épaisseur du fond
    d2 = 1; // diamètre boudins de blocage du haut
    l2 = La - 2; // longueur de boudins
    c1 = 6; // largeur des fentes
    dbo = (2*ep)+2; // débords
    bor = La + dbo; // largeur des débords
    
/* MODELISATIONS */

    // LA PLACE DE L'AIMANT
    module extrudaimant() { translate([0,0,Esocle]) color("grey")
        linear_extrude(height = Haimant, center = false, convexity = 10)
                import(file = "coffret-aimant-V00-01-16-01-21.dxf", layer = "01-blocaimant");
        }

    // LE COFFRAGE
    // Parois (cube extrudé par l'aimant)
    module cubeparois() { color("firebrick")
        linear_extrude(height = Hparois, center = false, convexity = 10)
                import(file = "coffret-aimant-V00-01-16-01-21.dxf", layer = "02-blocparois");
        }  
       
    // La fente des coins
    module fentecoins() { color("yellow")
        linear_extrude(height = Hx, center = false, convexity = 10)
                import(file = "coffret-aimant-V00-01-16-01-21.dxf", layer = "03-coins");
        }  

    // Le débord intérieur
    module debordext() { translate([0,0,Hbois])color("yellow")
        linear_extrude(height = Edebord, center = false, convexity = 10)
                import(file = "coffret-aimant-V00-01-16-01-21.dxf", layer = "04-debord-ext");
        }        
        
        // Le débord haut par extrusion
    module debordH() { translate([0,0,Haimant + Esocle]) color("yellow")
        linear_extrude(height = Hx, center = false, convexity = 10)
                import(file = "coffret-aimant-V00-01-16-01-21.dxf", layer = "06-percehaut");
        }        

/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        cubeparois();
        
        debordext();
	}
		fentecoins();
        extrudaimant();
        debordH();
}

/* FIN DE DOCUMENT */