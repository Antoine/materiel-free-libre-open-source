/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. ce quoi s'agit-il ? 
   #########################
*  Un coffret pour y placer un aimant
*  en forme de parallépidpède rectangle
*  de dimension L25,4 x P25,4 x H12,7 mm
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARAMETRES REGLABLES */
ecart = 0.3; //écart de précision versus théorie
epfonds = 0.7; //épaisseur des fonds
epbords = 0.7; //épaisseur des bords
epbois = 10; //épaisseur de la plaque support
daim = 25 +ecart; //diamètre de l'aimant

/* MODELISATIONS */
    /* Partie 1: le réceptacle de l'aimant*/
    /* Un cylindre excavé */
    module coffret() {
        difference() {
            union() {
                color("red")
                translate([0,0,((epbois-ecart)/2)])
                cylinder(h = (epbois-ecart), d =daim+(2*epbords), center = true);
                }
            color("pink") 
            translate([0,0,(((epbois-ecart)-epfonds)/2)+epfonds]) 
            cylinder(h = (epbois-ecart)-epfonds, d = (daim), center = true);
            }
        }
        
    /* Partie 2: le boitier bloqueur*/
    /* Un cylindre excavé avec un chapeau*/
    module boitier() {
        difference() {
            union() {
                color("blue")
                translate([0,0,((epbois+epfonds)/2)])
                cylinder(h = (epbois+epfonds), d = daim+(4*epbords)-ecart, center = true);
                
                color("grey")
                translate([0,0,(epfonds/2)])
                cylinder(h = epfonds, d = daim+(2*epbords)+epbois, center = true);
                }
            color("red") 
            translate([0,0,((epbois)/2)+epfonds]) 
            cylinder(h = epbois, d = (daim+(2*epbords)), center = true);
            }
        }

    module pousseur() {
        color("violet") 
        translate([0,0,epfonds/2]) 
        cylinder(h = epfonds, d =5, center = true);
        }
/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        translate([40,0,0]) coffret();
        boitier();
        }
		pousseur();
}

/* FIN DE DOCUMENT */