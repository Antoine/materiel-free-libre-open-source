/* lib-pliboo-fab-couvercleagripalaxe
*  version V15-05 devDraft-0 révision 00
*  ------------------------------
*  Guide pour axe avec ailettes
*  --
*  Mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################

/* LA TOUR DE SAROUMAN 
    * un tube de 73 mm de haut
    */
    module tube5mm() {
        {translate([0,0,0]) cylinder(h=73, r =2.85, center = false);}
    }

    module tube7mm() {
        {translate([0,0,0]) cylinder(h=73, r =3.5, center = false);}
    }
    
    module tourdiam5mm() {
        difference() {
            union(){
                tube7mm();
            }
            #tube5mm();
        }
    }

/* LES TROIS TOURS DE LA TERRE DU MILIEU 
    * trois tubes disposés sur une grille 10x,10y
    */
    module lestroistours() {
        translate([0,0,0]) tube7mm();
        translate([0,20,0]) tube7mm();
        translate([20,20,0]) tube7mm();        
    }
    
/* LES TROIS BASES */
    module embase() {
        cube(size = [5,20,3], center = true);
    }
    module embasegdcote() {
        cube(size = [5,25,3], center = true);
    }
    
    module lestroisbases() {
        rotate([0,0,0]) translate([0,3.25,0]) embase();
        rotate([0,0,-90]) translate([-20,3.25,0]) embase();
        rotate([0,0,-45]) translate([0,12,0]) embasegdcote();
    }
    
/* REMPART DES TROIS TOURS 
* trois fines plaques de 73mm de haut
* reliant les trois tours
*/    
    module plaqueptcotes() {
        cube(size = [1,16,73], center = false);
    }
    module plaquegdcote() {
        cube(size = [1,24,73], center = false);
    }    
    
    module lestroisremparts()  {
        rotate([0,0,0]) translate([0,3.25,0]) plaqueptcotes();
        rotate([0,0,-90]) translate([-20,3.25,0]) plaqueptcotes();
        rotate([0,0,-45]) translate([0,3.250,0]) plaquegdcote();
    }
    
/* ANTIMATIERE 
    * excavation de matières dans les trois cylindres
    * pour faire des tubes
    */     
     module lestroistubes() {
        translate([0,0,0]) tube5mm();
        translate([0,20,0]) tube5mm();
        translate([20,20,0]) tube5mm();        
    }   
    
/* TROIS TOURS ET SES REMPARTS 
    * le trépied finalisé
    */
    module trepied() {
        difference() {
            union() {
                lestroistours();
                lestroisremparts();
                lestroisbases();
            }
            #lestroistubes();
        }
     }
/* #########################
   2.  MISE EN FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */
        trepied();
/* FIN DE DOCUMENT */