/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* AILETTES */
     module patte1() {
        cube(size = [45,1,1], center = false);
    }
    
    module patte2() {
        translate([0,0,1]) cube(size = [30,1,1], center = false);
    }
    
    module patte3() {
        translate([0,0,2]) cube(size = [25,1,1], center = false);
    }
    
    module patte4() {
        translate([0,0,3]) cube(size = [15,1,1], center = false);
    }
    
    module patte5() {
        translate([0,0,4]) cube(size = [10,1,1], center = false);
    }
    
    module patte6() {
        translate([0,0,5]) cube(size = [2,1,1], center = false);
    }    
    
    
    rotate([0,0,0]) translate([0,3.25,0]) 
/* #########################
   2.  GENERATION DE LA FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
	}	
}

/* FIN DE DOCUMENT */