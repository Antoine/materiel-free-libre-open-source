/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. ce quoi s'agit-il ? 
   #########################
*  Un coffret pour y placer un aimant
*  en forme de parallépidpède rectangle
*  de dimension L25,4 x P25,4 x H12,7 mm
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARAMETRES REGLABLES */
    // Paramètres de l'aimant:
    La = 25.4; // largeur de l'aimant en mm
    Pa = La; // profondeur de l'aimant en mm
    Ha = 12.7+1; // hauteur de l'aimant en mm
    Hx = 100; // hauteur de trouée
    // Paramètres des parois du coffrage
    Hc = Ha + 0; // hauteur des paroies
    d1 = 2; // diamètre des boudins d'aspérité
    ep = 1; // épaisseur paroi
    epf = 0.7; // épaisseur du fond
    d2 = 1; // diamètre boudins de blocage du haut
    l2 = La - 2; // longueur de boudins
    c1 = 6; // largeur des fentes
    dbo = (2*ep)+2; // débords
    bor = La + dbo; // largeur des débords
    
/* MODELISATIONS */

    // L'AIMANT
    module aimant() {
        translate([0,0,Ha/2]) color("grey") cube(size = [La,Pa,Ha], center = true);
    }

    // LE COFFRAGE
    // Parois (cube extrudé par l'aimant)
        module cubeparois() {
        translate([0,0,Hc/2]) color("firebrick")  cube(size = [(La+(2*ep)),(Pa+(2*ep)),Hc], center = true);
        }
        
    // Le fond du coffrage
        module fond() {
        translate([0,0,-(epf/2)]) color("pink")  cube(size = [(La+(2*ep)),(Pa+(2*ep)),epf], center = true);
        }    
        
    // La fente des coins
        module fentecoins() {
        color("yellow")  cube(size = [c1,c1,Hx], center = true);
        }
    // Les 4 coins fendus
        module les4coins() {
            translate([La/2,Pa/2,Ha/2]) fentecoins();
            translate([-La/2,Pa/2,Ha/2]) fentecoins();
            translate([-La/2,-Pa/2,Ha/2]) fentecoins();
            translate([La/2,-Pa/2,Ha/2]) fentecoins();
            }
     
    // Les débords extérieurs
        module bords() {
            translate([-bor/2,-bor/2,Ha]) color("green")  cube(size = [bor,bor,epf], center = false);
            }
    // Les débords intérieurs = une excavassion
        module debordint() {
            translate([0,0,(Hx/2)+Ha]) color("green")  cube(size = [La-2,Pa-2,Hx], center = true);
            }
            

/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        cubeparois();
        fond();
        bords();
	}
		les4coins();
        aimant();
        debordint();
}

/* FIN DE DOCUMENT */