/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARAMETRES REGLABLES */
// ##########"
ep= 0.7; //epaisseur de la plaque 3D
h=10; // epaisseur du bois

/* DÉFINITION DES FORMES */
    module fond() {
        linear_extrude(height = ep, center = false, convexity = 10)
                import(file = "ABA-32a-12b-Maximized.dxf", layer = "0205plaquehaut22");
        }
    module coeur() {
        linear_extrude(height = h + ep, center = false, convexity = 10)
                import(file = "ABA-32a-12b-Maximized.dxf", layer = "0206coeurplaque2");
        }
    module tetons() {
        linear_extrude(height = h/2 + ep, center = false, convexity = 10)
                import(file = "ABA-32a-12b-Maximized.dxf", layer = "0207tetonsplaque2");
        }
    module teton() {
        cylinder(h=h/2 + ep, d=5, center= false);
        }
    module tetons2() {
        translate([160,36,0]) teton();
        translate([160,-36,0]) teton();
        translate([105,22,0]) teton();
        translate([105,-22,0]) teton();
        }
/* #########################
   2.  APPARITION A L'ECRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        fond();
        coeur();
        tetons2();
	}  
   
}

/* FIN DE DOCUMENT */