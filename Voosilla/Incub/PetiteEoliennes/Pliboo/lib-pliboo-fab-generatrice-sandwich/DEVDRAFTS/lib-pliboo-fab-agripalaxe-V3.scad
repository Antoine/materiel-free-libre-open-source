/* lib-pliboo-fab-agripalaxe
*  version V15-05 devDraft-0 révision 00
*  ------------------------------
*  Guide pour axe avec ailettes
*  --
*  Mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARTIE BASSE */
    module bloccylindre() {
        translate([0,0,0])  rotate([0,0,0]) cylinder(h=10, r=20, center = false);
    }
    
    module passagevis() {
        #translate([0,0,0])  rotate([0,0,90]) cube(size = [45,4,8], center = true);
    }
    
    module lalecrou() {
        translate([0,0,0])  rotate([0,0,0]) cube(size = [7,4,8], center = true);
    }
    module deuxtrousecrous() {
        #translate([0,-10,0]) lalecrou(center = true);
        #translate([0,10,0]) lalecrou(center = true);
    }
    module machinquisert() {
        difference(){
            union(){
                bloccylindre();
            }
            #translate([0,0,6]) passagevis(center=false);
            #translate([0,0,6]) deuxtrousecrous(center=false);
        }
    }
module cubic() {
        translate([0,0,5])  rotate([0,0,0]) cube(size = [30,15,10], center = true);
    }

module troudecrou() {
        #translate([10,0,6.5])  rotate([0,0,0]) cube(size = [3,7,7], center = true);
    }
    
module troudevis() {
        #translate([0,0,6.5])  rotate([0,90,0]) cylinder(h=40, r=2, center = true);
    }

module blocserrage() {
        difference() {
            union() {
                cubic();
            }
            troudecrou();
            troudevis();
        }
    }

/* UN SOCLE TROUE PAR 4 TROUS 10x,10y */
    module trou() 
        {#translate([0,0,-5]) cylinder(h = 70, r = 2.85, center = false);}
    module trous() 
    {
    translate([10,10,0]) trou();
    translate([-10,10,0]) trou();
    translate([-10,-10,0]) trou();
    translate([10,-10,0]) trou();
    }
    module socle() 
    {translate([0,0,0]) cylinder(h = 1, r = 20, center = true);}
    

   
    module socleatrous() {
        difference(){
            union(){
                socle();
            }
            #trous();
        }
    }
    
    module troudumilieu() 
    {#translate([0,0,5]) cylinder(h = 20, r = 5, center = true);}
    
/* #########################
   2.  MISE EN FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */
    difference(){
        union(){
             machinquisert();
        }
        #troudumilieu();
        #trous();
    }
   
    
/* FIN DE DOCUMENT */