/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   0. ce quoi s'agit-il ? 
   #########################
*  Un coffret pour y placer un aimant
*  en forme de parallépidpède rectangle
*  de dimension L25,4 x P25,4 x H12,7 mm
*/
    

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARAMETRES REGLABLES */
H=9;//hauteur des bords
e=1;//épaisseur du fond
    
/* MODELISATIONS */

    module bord1() { translate([0,0,0]) color("grey")
        linear_extrude(height = H, center = false, convexity = 10)
                import(file = "coffret-bobine-V00-01-16-01-21.dxf", layer = "00-bord-1");
        }

    module bord2() { translate([0,0,0]) color("grey")
        linear_extrude(height = H, center = false, convexity = 10)
                import(file = "coffret-bobine-V00-01-16-01-21.dxf", layer = "00-bord-2");
        }
    module centre() {
        difference(){
            union(){
                bord2();
            }
            bord1();
        }
    }
    
    
    module fond(){ translate([0,0,0]) color("yellow")
        linear_extrude(height = e, center = false, convexity = 10)
                import(file = "coffret-bobine-V00-01-16-01-21.dxf", layer = "00-bord-4");
        }

/* #########################
   2.  ASSEMBLAGE ET RENDU ÉCRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        centre();
        fond();
	}
		
}

/* FIN DE DOCUMENT */