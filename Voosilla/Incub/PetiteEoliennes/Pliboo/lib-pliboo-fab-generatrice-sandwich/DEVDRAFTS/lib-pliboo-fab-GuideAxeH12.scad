/* lib-pliboo-fab-GuideAxeH12
*  version V15-05 devDraft-0 révision 00
*  ------------------------------
*  Guide pour axe avec ailettes
*  --
*  Mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* 1.1 LA TOUR DU MILIEU (Mais où est Frodon ?) */

module tubeinterieur()
		{difference()
				{union()
					{cylinder(h = 12, r = 6, center = false);
					}/*fin de fonction union*/
				 #cylinder(h = 80, r = 4.45, center = true);
				}/* fin de fonction difference */	
		}/* fin de module */

/* 1.2 LES AILETTES DE FUSEE (partons en voyage interstellaire !)*/
/* 1.2.1 UNE PLAQUE */
module mur(){translate([-1.5,5,0]) 
            cube(size = [1,12,7], center = false);}

/* 1.2.2 UN BIAI */
module ailette(){#rotate(a=60, v=[1,0,0]) translate([0, 14, 0]) cube(size = [4,10,30], center = true);}

/* 1.2.3 L'AILETTE */

module murcroc()
		{difference()
				{union()
					{mur();
					}/*fin de fonction union*/
				 #ailette();
				}/* fin de fonction difference */	
		}/* fin de module */

/* 1.2.4 LA COURONNE AILEE */
module couronne() {
	for ( i = [0 : 3] )
	{
    rotate( i * 360 / 4, [0, 0, 1])
    translate([0, 0, 0])
    murcroc();
	}
}

/* 1.3 LES QUATRE TROUS DE FIXATION */
/* 1.3.1 TROUS */
module trou() 
{#translate([0,0,-5]) cylinder(h = 70, r = 2.85, center = false);}
/* 1.3.2 QUATRE TROUS EN (10n,10n)*/
module 4trous() 
{
translate([10,10,0]) trou();
translate([-10,10,0]) trou();
translate([-10,-10,0]) trou();
translate([10,-10,0]) trou();
}
module socle () 
{translate([0,0,0]) cylinder(h = 1, r = 20, center = true);}

module troudumilieu () 
{#translate([0,0,0]) cylinder(h = 80, r = 4.45, center = true);}


/* #########################
   2.  MISE EN FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
		tubeinterieur();
		couronne();
		socle();
	}
		4trous();
        troudumilieu (); 
}

/* FIN DE DOCUMENT */