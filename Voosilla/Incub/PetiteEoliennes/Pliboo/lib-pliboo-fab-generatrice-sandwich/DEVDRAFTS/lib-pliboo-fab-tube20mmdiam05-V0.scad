/* lib-pliboo-fab-tub20mmdiam05
*  version V15-05 devDraft-0 révision 00
*  ------------------------------
*  Guide pour axe avec ailettes
*  --
*  Mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################

/* LE TUBE DE PRINTEMPS 
    * un tube de 20mm de haut
    * de 05mm de diamètre intérieur et 07 extérieur
    */
    module cylindre5mm() {
        {translate([0,0,0]) cylinder(h=20, r =2.85, center = false);}
    }

    module cylindre7mm() {
        {translate([0,0,0]) cylinder(h=20, r =3.5, center = false);}
    }
    
    module tour5mm() {
        difference() {
            union(){
                cylindre7mm();
            }
            #cylindre5mm();
        }
    }

/* #########################
   2.  MISE EN FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */
        tour5mm();
/* FIN DE DOCUMENT */