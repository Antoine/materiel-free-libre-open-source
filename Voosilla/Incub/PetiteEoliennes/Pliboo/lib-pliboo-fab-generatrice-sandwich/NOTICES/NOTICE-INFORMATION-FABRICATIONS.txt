Section 4.2 / Licence CERN-OHL 1.2

Point(s) de contact permettant de recevoir des 
informations à propos des produits fabriqués
(voir section 4.2 de la licence CERN-OHL 1.2) :

Merci de prendre contact par courriel avec 
l'un de ces adresses :

 smallwindturbineproj[P01NT]contactor[CH3Z]gmail[P01NT]com
ou bien
 voosilla-incub-contact[CH3Z]confluence[P01NT]listes.vox.coop
 
 en remplaçant :
 * [P01NT] par un point (.)
 * [CH3Z] par le signe arobase (@)

