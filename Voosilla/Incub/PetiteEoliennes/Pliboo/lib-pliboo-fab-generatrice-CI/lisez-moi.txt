*************************************************
        Cette documentation fait partie du projet
        Pliboo, prototype de système de petite
        éolienne du projet expérimental Voosilla.
        Cette domentation concerne l'un des composants
        du projet Pliboo. 
        
        Pliboo inclus tout son processus de 
        développement ainsi que le développement 
        de ses composants.       
        
*************************************************
        [fr]
        --
        Pliboo et ses composants (dont celui-ci),
        sont mis à disposition sous licence 
        CERN OHL V 1.2
        --
        AUTEUR(S) : 
        Le projet VOOSILLA et,
        4k5ADllF0cok6 4kHq.ruL70FDI [1]
        Mai 2015
        --
        Cette documentation décrit de 
        l'Open Hardware [2]. Elle est mise à 
        disposition selon les termes de 
        la licence CERN OHL v.1.2. 

        Vous pouvez redistribuer et modifier cette
        documentation sous les termes de la licence
        CERN OHL v. 1.2 (http://ohwr.org/cernohl).
         
        Cette documentation est distribuée
        SANS AUCUNE GARANTIE D'AUCUNE SORTE, 
        EXPRESSE OU IMPLICITE, Y COMPRIS QUANT À 
        SA COMMERCIALISATION,  SA QUALITÉ 
        MARCHANDE, L'ABSENCE DE DÉFAUTS OU 
        D'ERREURS, SON EFFICACITÉ, ET SON APTITUDE 
        À REMPLIR UNE FONCTION DÉTERMINÉE. Merci 
        de consulter la licence CERN OHL v1.2. 
        pour connaître les conditions qui 
        s'appliquent.

        Vous devriez avoir reçu une copie de la 
        licence CERN OHL avec cet Open Hardware. 
        Le cas échéant, consultez
        <http://ohwr.org/cernohl> 

        EMPLACEMENT DE CETTE DOCUMENTATION
        Voir
        <https://fr-voosilla.ouvaton.org>
        et rechercher les sous-liens appropriés.

*************************************************

 AUTRES DESCRIPTIFS
 Pliboo et cette documentation, sont livrés
 avec d'autres documentations descriptives.
  
*************************************************
        NOTE
        
        [1] :  Les identités sont données après 
        avoir été cryptées avec la fonction 
        crypt PHP selon le format Prénom Nom (en
        respectant les majuscules et minuscules).
        
        [2] : Open Hardware : Matériel mise à 
        disposition selon les termes de licences 
        d'utilisation dites « ouvertes »
*************************************************
