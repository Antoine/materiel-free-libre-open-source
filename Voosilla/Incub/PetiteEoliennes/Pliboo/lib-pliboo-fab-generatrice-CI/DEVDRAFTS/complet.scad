/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* DISQUES HAUT ET BAS */
// Paramètres
// e = épaisseur du disque
// d = diamètre du disque
    for ( e = 10 );
    for ( d = 120);
// 


/* UNE ENTRETOISE EN FORME DE CROIX */
        
    module panneau() {
        cube(size = [2,20,30], center = true);
    }
    
    module croix() {
        panneau();
        rotate(a=90, v=[0,0,1]) panneau();
    }
    
    module tubeext7mm() {
        cylinder(h = 30, r = 3.5, center = true);
    }
    

module tubeinterieur()
		{difference()
				{union()
					{cylinder(h = 25, r = 6, center = false);
					}/*fin de fonction union*/
				 #cylinder(h = 25, r = 4.15, center = false);
				}/* fin de fonction difference */	
		}/* fin de module */

/* 1.2 LES AILETTES DE FUSEE (partons en voyage interstellaire !)*/
/* 1.2.1 UNE PLAQUE */
module mur(){translate([-1.5,5,0]) 
            cube(size = [3,15,15], center = false);}

/* 1.2.2 UN BIAI */
module ailette(){#rotate(a=32, v=[1,0,0]) translate([0, 20, 0]) cube(size = [4,10,30], center = true);}

/* 1.2.3 L'AILETTE */

module murcroc()
		{difference()
				{union()
					{mur();
					}/*fin de fonction union*/
				 #ailette();
				}/* fin de fonction difference */	
		}/* fin de module */

/* 1.2.4 LA COURONNE AILEE */
module couronne() {
	for ( i = [0 : 3] )
	{
    rotate( i * 360 / 4, [0, 0, 1])
    translate([0, 0, 0])
    murcroc();
	}
}

/* 1.3 LES QUATRE TROUS DE FIXATION */
/* 1.3.1 TROUS */
module trou() 
{#translate([0,0,-5]) cylinder(h = 70, r = 2.65, center = false);}
/* 1.3.2 QUATRE TROUS EN (10n,10n)*/
module 4trous() 
{
translate([10,10,0]) trou();
translate([-10,10,0]) trou();
translate([-10,-10,0]) trou();
translate([10,-10,0]) trou();
}
module socle () 
{translate([0,0,0]) cylinder(h = 3, r = 20, center = true);}

module troumilieu()
{#translate([0,0,0]) cylinder(h = 40, r = 2.75, center = true);}


/* #########################
   2.  MISE EN FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
		croix();
        tubeext7mm();
	}
		troumilieu();
}

/* FIN DE DOCUMENT */