/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARAMETRES REGLABLES */
// ##########"

H0=10; //Hauteur du socle
H3=4.5; //épaisseur d'un écrou
H32=H0-H3;//Hauteur du bas des écrous du haut par rapport à y=0
H4=4; //Diamètre de la vis de fixation aux plaques de fer = hauteur de la fente
H41=3;//Position du bas de la fente de passage de la vis de fixations au barres, par rapport au socle
H5=H0+H41; //Position y du bas de la fente
H6=H0+H41+H4+3;//Hauteur des bords de la fente à partir de y=0


/* DÉFINITION DES FORMES */
    module socle() {
        linear_extrude(height = H0, center = false, convexity = 10)
                import(file = "FixeurRegleur-v01.dxf", layer = "Socle");
        }
    
    // TROUS POUR LES ECROUS
    module ecrou() {
        linear_extrude(height = H3, center = false, convexity = 10)
                import(file = "FixeurRegleur-v01.dxf", layer = "EcrouExt");
        }
        
    module ecrous() {
        union(){
            translate([0,0,0])color("yellow") ecrou();
        translate([0,0,H32]) color("yellow") ecrou();
            }
        }
        
     // TROUS PAS DE VIS DES ECROUS
     module vissageh() {
        linear_extrude(height = H0+10, center = false, convexity = 10)
                import(file = "FixeurRegleur-v01.dxf", layer = "EcrouTrous");
        }

    // FIXATION SUR LES BARRES DE FER
    module vissage() {
        linear_extrude(height = H4, center = false, convexity = 10)
                import(file = "FixeurRegleur-v01.dxf", layer = "Vissage");
        }        
        
    module bords() {
        linear_extrude(height = H6, center = false, convexity = 10)
                import(file = "FixeurRegleur-v01.dxf", layer = "Bords");
        }
    // TROU DE L'AIMANT 25x25
     module trouaimant() {
        linear_extrude(height = 30, center = false, convexity = 10)
                import(file = "FixeurRegleur-v01.dxf", layer = "TrouAimant");
        }



/* #########################
   2.  APPARITION A L'ECRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        color("yellowgreen") bords();
        color("violet") socle();
       
	}  
        color("yellow") ecrous();
        color("yellow") ecrous();
        translate([0,0,H5]) color("blue") vissage();
        vissageh();
        color("grey") trouaimant();
}

/* FIN DE DOCUMENT */