/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

// PARAMETRES REGLABLES en mm


module socle() {
    translate([0,0,0]) cube([80,28,8],true);
}

module troucentral() {
        translate([0,0,0]) cylinder(h=10, r=27, center = false);
    }
module troufix() {
        translate([3,4,-hole/2]) cylinder(h=hole, d=d2, center = false);
        
    }
module portion() {linear_extrude(height = 15, center = false, convexity = 10)
                import(file = "test.dxf", layer = "6-brasduC");}

module excav1() {
    linear_extrude(height = 10, center = false, convexity = 10)
                import(file = "test.dxf", layer = "6.1-excavbrasduC2");
    }
    
module excav2() {
    linear_extrude(height = 30, center = false, convexity = 10)
                import(file = "test.dxf", layer = "6.2-excavbrasduC");
    }    
/* #########################
   2.  GENERATION DE LA FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
      portion();  
	}	
    #excav1();
    #excav2();
}

/* FIN DE DOCUMENT */