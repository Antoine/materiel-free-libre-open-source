/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

// PARAMETRES REGLABLES
e = 3; //épaisseur
h1 = 4; // hauteur des rebords
L = 60; // largeur de la plaque
L1 = 30; // longueur du rebord1
L2 = 10; // côté du carré emporte pièce trou
L3 = 50; // longueur du rebord3
L4 = 10; // longueur du rebord4
hole = 100; // hauteur de trou

module embase() {
        translate([-(L)/2,0,(e/2)]) cube(size = [L,L,e], center = true);
    }
module rebord1() {
        translate([-e,-(L1)/2,0]) color("blue") cube(size = [e,L1,e+h1], center = false);
    }
module trou() {
        translate([0,0,-hole/2]) cube(size = [L2,L2,100], center = false);
    }
module rebord3() {
        translate([-e-L2-e,-(L3)/2,0]) color("green") cube(size = [e,L3,h1+e], center = false);
    }    
module rebord4() {
        color("pink") cube(size = [L4,e,e+h1], center = false);
    }
module troudevis() {
        cylinder(h=hole, d1=5, d2=5, center=true);
    }
module troucentral() {
        cylinder(h=hole, d1=20, d2=20, center=true);
    }
/* #########################
   2.  GENERATION DE LA FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        embase();
        rebord1();
        rebord3();
        translate([-L4,27,0]) rebord4();
        translate([-L4,-27-e,0]) rebord4();
	}	
    #translate([-L2-e,6,0]) trou();
    #translate([-L2-e,-L2-6,0]) trou();
    #translate([-20,-20,0]) troudevis();
    #translate([-20,20,0]) troudevis();
    #translate([-40,0,0]) troucentral();
}

/* FIN DE DOCUMENT */