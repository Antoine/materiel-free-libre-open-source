/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

    module machoire() {
        cube(size = [105,10,10], center = false);
    }
    
    module entremachoire() {
        cube(size = [40,10,10], center = false);
    }
    
    module aimant() {
        cube(size = [10,10,10], center = false);
    }
    
    module rangaimants() {
        cube(size = [10,10,10], center = false);
        translate([10,0,0]) cube(size = [10,10,10], center = false);
        translate([20,0,0]) cube(size = [10,10,10], center = false);
        translate([30,0,0]) cube(size = [10,10,10], center = false);
    }
        
    module bloc() {
        machoire();
        translate([0,0,10]) entremachoire();
        translate([0,0,20]) rangaimants();
        }
        
    
    

module troumilieu()
{#translate([0,0,0]) cylinder(h = 40, r = dint, center = true);}


/* #########################
   2.  MISE EN FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

    bloc();

/* FIN DE DOCUMENT */