/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

// PARAMETRES REGLABLES en mm
e = 1; //épaisseur
c1 = 10; // côté long de l'axe rectangulaire
c2 = 14; // côté court de l'axe rectangulaire
la = 12; // longueur de l'axe
r1 = 17; // rayon du plateau
d1 = 5; // diamètre du trou dans l'axe central
d2 = 1.8; // diamètre des 4 trous de fixation
hole = 100; // hauteur des trous

module bloc() {
    difference(){
        union(){
            cube([20,20,10],true);
            }
        #translate([-10,0,0]) cube([20,3,20],true);
    }
}

module troucentral() {
        translate([0,0,-hole/2]) cylinder(h=hole, d=d1, center = false);
    }
module troufix() {
        translate([3,4,-hole/2]) cylinder(h=hole, d=d2, center = false);
        
    }
/* #########################
   2.  GENERATION DE LA FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        
     bloc();   
	}	
    #troucentral();
    #troufix();
}

/* FIN DE DOCUMENT */