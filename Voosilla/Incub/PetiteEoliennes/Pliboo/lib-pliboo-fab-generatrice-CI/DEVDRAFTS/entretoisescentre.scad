/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARAMETRES REGLABLES */
// Diamètre du tube central
    dint = 4.15; //diam exterieur
    dext = 6.15; //diam intérieur
    
/* UNE ENTRETOISE EN FORME DE CROIX */
        
    module panneau() {
        cube(size = [2,20,30], center = true);
    }
    
    module croix() {
        panneau();
        rotate(a=90, v=[0,0,1]) panneau();
    }
    
    module tubeext7mm() {
        cylinder(h = 30, r = dext, center = true);
    }
    

module troumilieu()
{#translate([0,0,0]) cylinder(h = 40, r = dint, center = true);}


/* #########################
   2.  MISE EN FORME
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
		croix();
        tubeext7mm();
	}
		troumilieu();
}

/* FIN DE DOCUMENT */