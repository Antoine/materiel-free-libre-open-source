/* Ceci est une documentation 
*  d'un Composant du projet Pliboo
*  ------------------------------
*  Pliboo, est un système de petite éolienne
*  du projet Voosilla qui inclus
*  tout son processus de développement
*  ainsi que le développement de ses composants
*  --
*  Pliboo, incluant ce composant, 
*  est mis à disposition sous licence 
*  CERN OHL V 1.2
*  --
*  Auteur:
*  projet VOOSILLA et,
*  4k5ADllF0cok6 4kHq.ruL70FDI (Identité Cryptée)
*  Mai 2015
*  
*  Droits et Conditions d'utilisation resteints: 
*  voir fichier lisez-moi.txt
*  ------------------------------
*/

/* NOTE EXPLICATIVE
   Le document fonctionne en 2 partie:
   1. définition de modules nécessaires pour dessiner
      la pièce par ajout, soustraction, etc ...
   2. appel des modules définis en 1 pour
      réaliser la pièce
*/

/* #########################
   1. DEFINITION DE MODULES 
   #########################
*/

/* PARAMETRES REGLABLES */
// ##########"
ep= 0.7; //epaisseur de la plaque 3D
h0=15; // epaisseur du socle
h1=h0+12; // profondeur des bords
h3=5; // epaisseur ecrou
h4=3; // largeur du vissage
ma=h4+4; //marge haut du vissage

/* DÉFINITION DES FORMES */
    module bords() {
        linear_extrude(height = h1, center = false, convexity = 10)
                import(file = "FixeurRegleur.dxf", layer = "Bords");
        }
    module socle() {
        linear_extrude(height = h0, center = false, convexity = 10)
                import(file = "FixeurRegleur.dxf", layer = "Socle");
        }
    module ecrous() {
        linear_extrude(height = h3, center = false, convexity = 10)
                import(file = "FixeurRegleur.dxf", layer = "EcrouExt");
        }
    module vissage() {
        linear_extrude(height = h4, center = false, convexity = 10)
                import(file = "FixeurRegleur.dxf", layer = "Vissage");
        }
    module teton() {
        cylinder(h=h/2 + ep, d=5, center= false);
        }
    module tetons2() {
        translate([160,36,0]) teton();
        translate([160,-36,0]) teton();
        translate([105,22,0]) teton();
        translate([105,-22,0]) teton();
        }
/* #########################
   2.  APPARITION A L'ECRAN
   #########################
*/
/* choix de la définition à l'écran*/
$fn=100;

/* appel des fonctions */

difference(){
	union(){
        color("yellowgreen") bords();
        color("violet") socle();
        

	}  
   color("yellow") ecrous();
        translate([0,0,10]) color("yellow") ecrous();
        translate([0,0,h1-ma]) color("firebrick") vissage();
}

/* FIN DE DOCUMENT */